package com.test.roposo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by DeathStar on 02/04/16.
 */
public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Object> data;
    public static final int AUTHOR = 0;
    public static final int STORY = 1;
    Context context;

    public MyAdapter(ArrayList<Object> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case AUTHOR:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.author_item, parent, false);
                return new AuthorViewHolder(view);

            case STORY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.story_item, parent, false);
                return new StoryViewHolder(view);

            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if(viewHolder.getItemViewType() == STORY) {
            StoryViewHolder storyViewHolder = (StoryViewHolder) viewHolder;
            final Story story = (Story) data.get(position);
            storyViewHolder.title_story.setText(story.getTitle());
            storyViewHolder.likes.setText(story.getLikes_count() + " likes");
            storyViewHolder.comments.setText(story.getComment_count() + " comments");
            Glide.with(context).load(story.si).asBitmap().fitCenter()
                    .into(storyViewHolder.pic);
            if(story.isLike_flag()) {
                storyViewHolder.followStory.setText("Following");
            } else {
                storyViewHolder.followStory.setText("Follow");
            }
            storyViewHolder.followStory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(story.isLike_flag()) {
                        story.setLike_flag(false);
                    } else {
                        story.setLike_flag(true);
                    }
                    data.remove(position);
                    data.add(position, story);
                    notifyItemChanged(position);
                    Util.writeToFile(context, new Gson().toJson(data));
                }
            });


        } else {
            AuthorViewHolder authorViewHolder = (AuthorViewHolder) viewHolder;
            final Author author = (Author) data.get(position);
            Util.AUTHORS_MAP.put(author.getId(), author);
            authorViewHolder.title_author.setText(author.getUsername());
            Glide.with(context).load(author.image).asBitmap().fitCenter()
                    .into(authorViewHolder.pic);
            authorViewHolder.twitter_handle.setText(author.getHandle());
            authorViewHolder.twitter_followers.setText(author.getFollowers() + " followers");
            if(author.isFollowing) {
                authorViewHolder.follow.setText("Following");
            } else {
                authorViewHolder.follow.setText("Follow");
            }

            authorViewHolder.follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(author.isFollowing()) {
                        author.setIsFollowing(false);
                    } else {
                        author.setIsFollowing(true);
                    }
                    Log.e("MyAdapter", "position " + position);
                    data.remove(position);
                    data.add(position, author);
                    notifyItemChanged(position);
                    //context.sendBroadcast(intent);
                    Util.writeToFile(context, new Gson().toJson(data));
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(data.get(position) instanceof Author) {
            return AUTHOR;
        }else {
            return STORY;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class AuthorViewHolder extends ViewHolder {
        TextView title_author;
        ImageView pic;
        TextView twitter_handle;
        TextView twitter_followers;
        TextView follow;
        public AuthorViewHolder(View itemView) {
            super(itemView);
            this.title_author = (TextView) itemView.findViewById(R.id.title_author);
            this.twitter_handle = (TextView) itemView.findViewById(R.id.twitter_author);
            this.twitter_followers = (TextView) itemView.findViewById(R.id.followers_author);
            this.follow = (TextView) itemView.findViewById(R.id.follow);
            this.pic = (ImageView) itemView.findViewById(R.id.pic_author);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Log.e("Adapter", "onClick: "+getAdapterPosition());
                    ((MainActivity) context).onFragmentInteraction(
                            (Author) data.get(getPosition()), getAdapterPosition());
                }
            });
        }
    }

    public class StoryViewHolder extends ViewHolder {
        TextView title_story;
        TextView likes;
        TextView comments;
        ImageView pic;
        TextView followStory;
        public StoryViewHolder(final View itemView) {
            super(itemView);
            this.title_story = (TextView) itemView.findViewById(R.id.title_story);
            this.likes = (TextView) itemView.findViewById(R.id.likes_story);
            this.comments = (TextView) itemView.findViewById(R.id.comments_story);
            this.pic = (ImageView) itemView.findViewById(R.id.pic_story);
            this.followStory = (TextView) itemView.findViewById(R.id.follow_story);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity)context).onFragmentInteraction(
                            (Story) data.get(getPosition()), getAdapterPosition());
                }
            });
        }
    }
}
