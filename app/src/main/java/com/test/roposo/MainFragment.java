package com.test.roposo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainFragment extends Fragment {

    private OnFragmentInteractionListenerAuthor authorListener;
    private OnFragmentInteractionListenerStory storyListener;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager lm;
    MyAdapter adapter;
    private static String ARG_PARAM = "json";
    String json;
    ArrayList<Object> data;
    FollowListener followListener;

    public MainFragment() {}

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        followListener = new FollowListener();
        getContext().registerReceiver(followListener, new IntentFilter(Util.FOLLOW_ACTION));
        if (context instanceof OnFragmentInteractionListenerAuthor) {
            authorListener = (OnFragmentInteractionListenerAuthor) context;
            storyListener = (OnFragmentInteractionListenerStory) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            json = Util.readJsonFromFile(getActivity());
            data = Util.parseJson(json);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        lm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(lm);
        adapter = new MyAdapter(data, (Context) authorListener);

        recyclerView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        authorListener = null;
        getActivity().unregisterReceiver(followListener);
    }

    public interface OnFragmentInteractionListenerAuthor {
        void onFragmentInteraction(Author author, int position);
    }

    public interface OnFragmentInteractionListenerStory {
        void onFragmentInteraction(Story story, int position);
    }
    private class FollowListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra(Util.POSITION, -1);
            //Log.e("MainFragment", "onReceive: " + position );
            if(position != -1) {
                if(data.get(position) instanceof Author) {
                    Author author = (Author) data.get(position);
                    if(author.isFollowing) {
                        author.setIsFollowing(false);
                    } else {
                        author.setIsFollowing(true);
                    }
                    data.remove(position);
                    data.add(position, author);
                } else {
                    Story story = (Story) data.get(position);
                    if (story.isLike_flag()) {
                        story.setLike_flag(false);
                    } else {
                        story.setLike_flag(true);
                    }
                    data.remove(position);
                    data.add(position, story);
                }
                adapter.notifyItemChanged(position);

                Gson gson = new Gson();
                String json = gson.toJson(data);
                Util.writeToFile(context, json);
            }
        }
    }

}
