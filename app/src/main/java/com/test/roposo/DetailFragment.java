package com.test.roposo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailFragment extends Fragment {

    private Author author;
    private Story story;
    private int position;
    private static boolean isAuthor;
    public DetailFragment() {}

    public static DetailFragment newInstance(Author author, int position) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(Util.OBJECT, author);
        isAuthor = true;
        args.putInt(Util.POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetailFragment newInstance(Story story, int position) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        isAuthor = false;
        args.putSerializable(Util.OBJECT, story);
        args.putInt(Util.POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(isAuthor) {
                author = (Author) getArguments().getSerializable(Util.OBJECT);
            } else {
                story = (Story) getArguments().getSerializable(Util.OBJECT);
            }

            position = getArguments().getInt(Util.POSITION);
            //Log.e("DetailFrag", "onCreate: "+position );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = null;

        if(isAuthor) {
            view = inflater.inflate(R.layout.author_detail, container, false);
            TextView name = (TextView) view.findViewById(R.id.author_name);
            TextView about = (TextView) view.findViewById(R.id.about_author);
            TextView twitterHandle = (TextView) view.findViewById(R.id.twitter_author);
            TextView followers = (TextView) view.findViewById(R.id.followers_author);
            TextView seeMore = (TextView) view.findViewById(R.id.see_more);
            ImageView pic = (ImageView) view.findViewById(R.id.author_detail_pic);
            final TextView follow = (TextView) view.findViewById(R.id.follow);
            //TextView viewAll = (TextView) view.findViewById(R.id.view_all);
            name.setText(author.getUsername());
            about.setText(author.getAbout());
            twitterHandle.setText(author.getHandle());
            followers.setText(author.getFollowers() + " followers");
            Glide.with(getActivity()).load(author.getImage()).asBitmap().fitCenter()
                    .into(pic);
            if(author.isFollowing) {
                follow.setText("Following");
            } else {
                follow.setText("Follow");
            }

            seeMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(author.getUrl());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });

            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Util.FOLLOW_ACTION);
                    intent.putExtra(Util.POSITION, position);
                    //Log.e("DetailFragment", "position: " + position);
                    if(author.isFollowing) {
                        follow.setText("Follow");
                        author.setIsFollowing(false);
                    } else {
                        follow.setText("Following");
                        author.setIsFollowing(true);
                    }
                    getContext().sendBroadcast(intent);
                }
            });

        } else {
            view = inflater.inflate(R.layout.story_detail, container, false);
            TextView author = (TextView) view.findViewById(R.id.author);
            TextView title = (TextView) view.findViewById(R.id.title);

            TextView createdOn = (TextView) view.findViewById(R.id.created_on);
            TextView desc = (TextView) view.findViewById(R.id.desc);
            TextView likes = (TextView) view.findViewById(R.id.likes);
            TextView comments = (TextView) view.findViewById(R.id.comments);
            final TextView followStory = (TextView) view.findViewById(R.id.follow_story);
            ImageView pic = (ImageView) view.findViewById(R.id.pic);

            author.setText(Util.AUTHORS_MAP.get(story.getDb()).getUsername());
            createdOn.setText(story.getVerb());
            desc.setText(story.getDescription());
            likes.setText(story.getLikes_count() + " likes");
            comments.setText(story.getComment_count() + " comments");
            title.setText(story.getTitle());
            Glide.with(getActivity()).load(story.si).asBitmap().fitCenter()
                    .into(pic);
            if(story.isLike_flag()) {
                followStory.setText("Following");
            } else {
                followStory.setText("Follow");
            }

            followStory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Util.FOLLOW_ACTION);
                    intent.putExtra(Util.POSITION, position);
                    //Log.e("DetailFragment", "position: " + position);
                    if(story.isLike_flag()) {
                        followStory.setText("Follow");
                        story.setLike_flag(false);
                    } else {
                        followStory.setText("Following");
                        story.setLike_flag(true);
                    }
                    getContext().sendBroadcast(intent);
                }
            });
        }
        return view;
    }

}
