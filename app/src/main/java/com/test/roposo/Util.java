package com.test.roposo;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by DeathStar on 02/04/16.
 */
public class Util {

    public static HashMap<String, Author> AUTHORS_MAP = new HashMap<>();
    public static final String FOLLOW_ACTION = "com.test.roposo.follow.action";
    public static final String POSITION = "position";
    public static final String OBJECT = "object";



    public static ArrayList<Object> parseJson(String json) {
        String TAG = "JsonParse";
        ArrayList<Object> data = new ArrayList<>();
        try {
            JSONArray arr = new JSONArray(json);
            Gson gson = new Gson();
            for(int i=0;i<arr.length();i++) {
                JSONObject obj = arr.getJSONObject(i);
                if(obj.has("username")) {
                    Author author = gson.fromJson(obj.toString(), Author.class);
                    data.add(author);
                } else {
                    Story story = gson.fromJson(obj.toString(), Story.class);
                    data.add(story);
                }
            }

        } catch (JSONException e) {
            Log.e(TAG, "getDataFromFile: " + e.getMessage());
        }

        return data;
    }

    public static String readJsonFromFile(Context context) {
        File sdcard = Environment.getExternalStorageDirectory();
        File file = new File(sdcard, "/Download/iOS-Android Data.json");
        BufferedReader reader = null;
        String TAG = "ReadFile";
        StringBuilder sb = new StringBuilder();
        try {
//            reader = new BufferedReader(
//                    new InputStreamReader(context.getAssets().open("data.json")));
            reader = new BufferedReader(new FileReader(file));
            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                sb.append(mLine);
            }
        } catch (IOException e) {
            Log.e(TAG, "readJsonFromFile: " + e.getMessage());
            Toast.makeText(context, "Cannot read iOS-Android Data.json"+"\n"+
                    "Put the file in Download directory", Toast.LENGTH_LONG).show();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, "readJsonFromFile: " + e.getMessage());
                }
            }
        }
        return sb.toString();
    }

    public static void writeToFile(Context context, String data) {
        File sdcard = Environment.getExternalStorageDirectory();
        File file = new File(sdcard, "/Download/iOS-Android Data.json");
        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            pw.println(data);
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            Log.e("WriteFile", "writeJsonToFile: " + e.getMessage());
        } catch (IOException e) {
            Log.e("WriteFile", "writeJsonToFile: " + e.getMessage());
        }
    }

}
