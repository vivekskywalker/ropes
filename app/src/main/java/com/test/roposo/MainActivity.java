package com.test.roposo;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainFragment
        .OnFragmentInteractionListenerAuthor, MainFragment.OnFragmentInteractionListenerStory {

    MainFragment mainFragment;
    FragmentManager fm;
    //String data;
    boolean twoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //data = Util.readJsonFromFile(this);

        FrameLayout detailsFrame = (FrameLayout) findViewById(R.id.container_detail);
        if(detailsFrame != null) {
            twoPane = true;
        }

        fm = getSupportFragmentManager();
        mainFragment = MainFragment.newInstance();
        fm.beginTransaction().replace(R.id.container_main, mainFragment, "MAIN_FRAG").commit();
    }

    @Override
    public void onFragmentInteraction(Author author, int position) {
        if(!twoPane) {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(Util.OBJECT, (Author) author);
            intent.putExtra(Util.POSITION, position);
            startActivity(intent);
        } else {

            DetailFragment detailFragment = DetailFragment.newInstance(author, position);
            fm.beginTransaction().replace(R.id.container_detail, detailFragment, "DETAIL_FRAG").commit();
        }
    }

    @Override
    public void onFragmentInteraction(Story story, int position) {
        if(!twoPane) {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(Util.OBJECT, story);
            intent.putExtra(Util.POSITION, position);
            startActivity(intent);
        } else {

            DetailFragment detailFragment = DetailFragment.newInstance(story, position);
            fm.beginTransaction().replace(R.id.container_detail, detailFragment, "DETAIL_FRAG").commit();
        }
    }
}
