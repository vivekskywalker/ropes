package com.test.roposo;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class DetailActivity extends AppCompatActivity {

    FragmentManager fm;
    DetailFragment detailFragment;
    Author author;
    Story story;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Object object = getIntent().getSerializableExtra(Util.OBJECT);
        position = getIntent().getIntExtra(Util.POSITION, -1);
        if(object instanceof Author) {
            author = (Author) object;
            detailFragment = DetailFragment.newInstance(author, position);
            Log.e("DetailActivity", "onCreate: "+position );
        } else {
            story = (Story) object;
            detailFragment = DetailFragment.newInstance(story, position);
        }


        //Log.e("DetailActivity", "onCreate: "+position );
        fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.container_detail, detailFragment, "DETAIL_FRAG").commit();
    }
}
